//Configure Google Cloud provider
provider "google" {
  version     = "1.18"
  credentials = "${file("My First Project-3cf4a064dd21.json")}"
  project     = "unique-caldron-216513"
  region      = "${var.region}"
}

//Create Instance
module "gcp_instance" {
  source       = "./modules/instance"

  name         = "dockerjenkins"
  machine_type = "g1-small"
  disk_image   = "ubuntu-os-cloud/ubuntu-minimal-1804-lts"
}

output "jenkins_ip" {
  value = "${module.gcp_instance.external_ip}"
}

#  provisioner "local-exec" {
#    command = "echo ${module.gcp_instance.external_ip} >> var_out.txt"
#  }



//configuring firewall rules
resource "google_compute_firewall" "default" {
  name          = "allow-epam"
  network       = "default"
  source_ranges = ["46.229.218.224/32", "46.229.218.225/32"]
  
  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["8080"]
  }
}
