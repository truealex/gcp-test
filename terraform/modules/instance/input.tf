variable "name" {}

variable "zone" {
  default = "europe-north1-a"
}
variable "amount" {
  default = "1"
}

variable "machine_type" {}

variable "disk_image" {}
