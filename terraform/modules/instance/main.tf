resource "google_compute_instance" "instance" {
  count        = "${var.amount}"
  name         = "${var.name}"
  zone         = "${var.zone}"
  machine_type = "${var.machine_type}"

  boot_disk = {
    initialize_params {
      image    = "${var.disk_image}"
    }
  }

  network_interface = {
    network = "default"

    access_config = {
      //nat_ip will be generated here automatically
    }
  }

  service_account {
    scopes = ["userinfo-email", "compute-ro", "storage-ro"]
  }
}
